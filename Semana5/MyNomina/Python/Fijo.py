from Empleado import Empleado

class Fijo(Empleado):
    ''' Fijo (hereda de Empleado, atributos: EPS, Fondo de Pensión, Fondo de cesantías, 
        ARL, salario, horas extras, método: getPago(número del mes))
    '''
    __EPS__ = ""
    __Pension__ = ""
    __cesantias__= ""
    __arl__ = ""
    __salario__ = 0
    __horasExtras__ = 0
    
    def __init__(self, nombre, identificacion, codigoInt, banco, cuenta, mesFacturar, 
                  salario, horasExtras):
        super().__init__(nombre, identificacion, codigoInt, banco, cuenta, mesFacturar)
        self.__salario__ =  salario
        self.__horasExtras__ = horasExtras
        
    def getEps(self):
        return self.__eps__
   
    def setEps(self, eps):
        self.__eps__ = eps
        
    def getPension(self):
        return self.__pension__
   
    def setPension(self, pension):
        self.__pension__ = pension
        
    def getCesantias(self):
        return self.__cesantias__
   
    def setCesantias(self, cesantias):
        self.__cesantias__ = cesantias
        
    def getArl(self):
        return self.__arl__
   
    def setArl(self, arl):
        self.__arl__ = arl
        
    def getSalario(self):
        return self.__salario__
   
    def setSalario(self, salario):
        self.__salario__ = salario
    
    def getHorasExtras(self):
        return self.__horasExtras__
   
    def setHorasExtras(self, horasExtras):
        self.__horasExtras__ = horasExtras
        
    def getPago(self):
        return (self.__salario__*0.92) +(self.__horasExtras__*50000)