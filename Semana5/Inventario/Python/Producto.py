class Producto():
    __codigo__ = 0
    __nombre__ = ""
    __precio__ = 0
    __inventario__ = 0

    def __init__(self, codigo, nombre, precio, inventario) :
        self.__codigo__ = codigo
        self.__nombre__ = nombre
        self.__precio__ = precio
        self.__inventario__ = inventario

    def setCodigo(self, codigo):
        self.__codigo__ = codigo

    def getCodigo(self):
        return self.__codigo__

    def setNombre(self, nombre):
        self.__nombre__ = nombre

    def getNombre(self):
        return self.__nombre__

    def setPrecio(self, precio):
        self.__precio__ = precio

    def getPrecio(self):
        return self.__precio__

    def setInventario(self, inventario):
        self.__inventario__ = inventario

    def getInventario(self):
        return self.__inventario__