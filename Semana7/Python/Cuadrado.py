from FigurasGeometricas import FigurasGeometricas

class Cuadrado(FigurasGeometricas):
    __lado__ = 0.0
    
    
    def __init__(self, lado):
        super().__init__()
        self.__lado__ = lado
        
    def getLado(self):
        return self.__lado__
    
    def setLado(self, lado):
        self.__lado__ = lado
        
    
    def area(self):
        return self.__lado__**2
    
    def perimetro(self):
        return (self.__lado__)*4
    
    def pintar(self):
        print("Cuadrado de  lado "+str(self.__lado__))
        
    
    