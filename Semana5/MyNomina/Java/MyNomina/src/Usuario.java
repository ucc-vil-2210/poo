public class Usuario extends Persona {
    //Usuario (implementa de Persona, atributos: identificación, usuario, contraseña)
    private int identificacion;
    private String usuario;
    private String password;
    public Usuario(String nombre, String apellido, int identificacion, int celular, int identificacion2, String usuario,
            String password) {
        super(nombre, apellido, identificacion, celular);
        identificacion = identificacion2;
        this.usuario = usuario;
        this.password = password;
    }
    public int getIdentificacion() {
        return identificacion;
    }
    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
