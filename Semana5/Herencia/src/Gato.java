public class Gato extends Animal {
    private String nombre;
    private String raza;
    private String dueno;

    public Gato(String color, int edad, double altura, String nombre, String raza, String dueno) {
        super(color, edad, altura);
        this.nombre = nombre;
        this.raza = raza;
        this.dueno = dueno;

    }

    public String getDueno() {
        return dueno;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public void maullar() {
        System.out.println("Miauuu miauu..");
    }

    public void asear() {
        System.out.println(nombre + " esta aseando");
    }
}
