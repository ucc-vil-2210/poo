public class Animal {
    private String color;
    private int edad;
    private double altura; // La altura sera en cm

    public Animal(String color, int edad, double altura) {
        this.color = color;
        this.edad = edad;
        this.altura = altura;
    }

    public Animal() {
        this.color = "";
        this.edad = 0;
        this.altura = 0;
    }

    public void comer(String comida) {
        System.out.println("El animal come" + comida);
    }

    public void dormir(boolean dormido) {
        if (dormido) {
            System.out.println("El animal esta Dormido ZZZzzzz...");
        } else {
            System.out.println("El animal esta Despierto");
        }
    }

    public int getEdad() {
        return this.edad;
    }

    public String getColor() {
        return this.color;
    }

    public double getAltura() {
        return this.altura;
    }

    public void sonido() {
        System.out.println("el animal emite sonido");
    }
}
