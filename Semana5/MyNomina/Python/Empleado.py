from Persona import Persona
class Empleado (Persona):
    """Empleado (implementa de Persona, atributos: código interno, banco, 
        número de cuenta, mes a facturar, método: getPago(número del mes)) """
    __codigoInt__ = ""
    __banco__ = ""
    __cuenta__ = 0
    __mesFacturar__ = 0
    
    def __init__(self, nombre, identificacion, codigoInt, banco, cuenta, mesFacturar):
        super().__init__(nombre, identificacion)
        self.__codigoInt__ = codigoInt
        self.__banco__ = banco
        self.__cuenta__ = cuenta
        self.__mesFacturar__ =  mesFacturar
    
    def getCodigoInt(self):
        return self.__codigoInt__
   
    def setCodigoInt(self, codigoInt):
        self.__codigoInt__ = codigoInt
    
    def getBanco(self):
        return self.__banco__
   
    def setBanco(self, banco):
        self.__banco__ = banco
        
    def getCuenta(self):
        return self.__cuenta__
   
    def setCuenta(self, cuenta):
        self.__cuenta__ = cuenta
        
    def getMesFacturar(self):
        return self.__mesFacturar__
   
    def setMesFacturar(self, mesFacturar):
        self.__mesFacturar__ = mesFacturar
    
    def getPago(self):
        return 0