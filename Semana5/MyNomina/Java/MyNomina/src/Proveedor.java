public class Proveedor extends Empleado {
    //Proveedor (hereda de Empleado, atributos: horas trabajadas, valor de la hora, 
    //método: getPago(número del mes))
    private int horasTrabajadas;
    private int valorHora;
    
    
    public Proveedor(String nombre,  int identificacion, String codigoInt, String banco,
            int numCuenta, int mesAFacturar, int horasTrabajadas, int valorHora) {
        super(nombre,  identificacion,  codigoInt, banco, numCuenta, mesAFacturar);
        this.horasTrabajadas = horasTrabajadas;
        this.valorHora = valorHora;
    }


    @Override
    public int getPago(int mes) {
        // TODO Auto-generated method stub
        return horasTrabajadas*valorHora;
    }


    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }


    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }


    public int getValorHora() {
        return valorHora;
    }


    public void setValorHora(int valorHora) {
        this.valorHora = valorHora;
    }
}
