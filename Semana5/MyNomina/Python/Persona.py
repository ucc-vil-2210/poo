#Persona (atributos: nombre, apellido, identificación, celular) 
class Persona:
    __nombre__ = ""
    __apellido__ = ""
    __identificacion__ = 0
    __celular__ = 0
    
    def __init__(self, nombre,identificacion):
        self.__nombre__ = nombre
        self.__identificacion__ =  identificacion
        
    def getNombre(self):
        return self.__nombre__
   
    def setNombre(self, nombre):
        self.__nombre__ = nombre
    
    def getApellido(self):
        return self.__apellido__
   
    def setApellido(self, apellido):
        self.__apellido__ = apellido
        
    def getIdentificacion(self):
        return self.__identificacion__
   
    def setIdentificacion(self, identificacion):
        self.__identificacion__ = identificacion
        
    def getCelular(self):
        return self.__celular__
   
    def setCelular(self, celular):
        self.__celular__ = celular