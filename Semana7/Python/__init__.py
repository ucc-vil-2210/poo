from Cuadrado import Cuadrado
from Rectangulo import Rectangulo
from TrianguloRectangulo import TrianguloRectangulo

if __name__ =="__main__":
    c1=Cuadrado(10)
    print("area"+ str(c1.area()))
    print("Perimetro"+ str(c1.perimetro()))
    t1=TrianguloRectangulo(5,4)
    print("area"+ str(t1.area()))
    print("Perimetro"+ str(t1.perimetro()))
    r1=Rectangulo(5,4)
    print("area"+ str(r1.area()))
    print("Perimetro"+ str(r1.perimetro()))