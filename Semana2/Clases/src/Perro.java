package Clases.src;

public class Perro {
    public int edad;
    public String nombre;
    public String color;

    public Perro(int edad, String nombre, String color){
        this.edad = edad;
        this.nombre = nombre;
        this.color = color;
    }

    public void ladrar(){
        System.out.println(this.color+" Guauuu guauuu...");
    }

    public int edadMeses(){
        return this.edad*12;
    }
}