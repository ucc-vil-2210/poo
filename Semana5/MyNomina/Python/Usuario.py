from Persona import Persona

class Usuario(Persona):
    #Usuario (implementa de Persona, atributos: identificación, usuario, contraseña)
    __identificacion__ = 0
    __usuario__ = ""
    __password__ = ""
    
    def __init__(self, nombre, identificacion, identificacion2, usuario, password):
        super().__init__(nombre, identificacion)
        self.__identificacion__= identificacion2
        self.__usuario__ = usuario
        self.__password__ = password

    def getIdentificacion(self):
        return self.__identificacion__
   
    def setIdentificacion(self, identificacion):
        self.__identificacion__ = identificacion
    
    def getUsuario(self):
        return self.__usuario__
   
    def setUsuario(self, usuario):
        self.__usuario__ = usuario
    
    def getPassword(self):
        return self.__password__
   
    def setPassword(self, password):
        self.__password__ = password
    