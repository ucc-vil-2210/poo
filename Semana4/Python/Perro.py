from typing_extensions import Self


class Perro:
    __id__ = 0  # _Clase__id --> _Perro__id 
    __nombre__ = ""
    __color__  = ""
    __edad__ = 0
    __altura__ = 0.0
    
    def __init__(self, id, nombre, color, edad, altura):
        self.__id__ = id
        self.__nombre__ = nombre
        self.__color__ = color
        self.__edad__ = edad
        self.__altura__ = altura
        
    def getId(self):
        return self.__id__
    
    def getNombre(self):
        return self.__nombre__
    
    def getColor(self):
        return self.__color__
    
    def getEdad(self):
        return self.__edad__
    
    def getAltura(self):
        return self.__altura__
    
    def setNombre(self, nombre):
        self.__nombre__ = nombre
        
    def setColor(self, color):
        self.__color__ = color
        
    def setEdad(self, edad):
        self.__edad__ = edad
        
    def setAltura(self, altura):
        self.__altura__ = altura
        
    
    
    