from Empleado import Empleado

class Proveedor(Empleado):
    #Proveedor (hereda de Empleado, atributos: horas trabajadas, valor de la hora, 
    #método: getPago(número del mes))
    __horasTrabajadas__ = 0
    __valorHora__ = 0

    def __init__(self, nombre, identificacion, codigoInt, banco, cuenta, mesFacturar, 
                  horasTrabajadas, valorHora):
        super().__init__(nombre, identificacion, codigoInt, banco, cuenta, mesFacturar)
        self.__horasTrabajadas__ =  horasTrabajadas
        self.__valorHora__ = valorHora
        
    def getHorasTrabajadas(self):
        return self.__horasTrabajadas__
   
    def setHorasTrabajadas(self, horasTrabajadas):
        self.__horasTrabajadas__ = horasTrabajadas
        
    def getValorHora(self):
        return self.__valorHora__
   
    def setValorHora(self, valorHora):
        self.__valorHora__ = valorHora
    
    def getPago(self):
        return self.__horasTrabajadas__ * self.__valorHora__