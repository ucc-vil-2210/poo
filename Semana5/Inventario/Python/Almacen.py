from Producto import Producto

class Almacen(Producto):
    __productos__ = dict()
    
    def __init__(self):
        P1 = Producto(1,"Manzanas",6000.0,25)
        P2 = Producto(2,"Limones",2500.0,15)
        P3 = Producto(3,"Peras",2700.0,33)
        P4 = Producto(4,"Arandanos",9300.0,34)
        P5 = Producto(5,"Tomates",2100.0,42)
        P6 = Producto(6,"Fresas",4100.0,10)
        P7 = Producto(7,"Helado",4500.0,41)
        P8 = Producto(8,"Galletas",500.0,8)
        P9 = Producto(9,"Chocolates",4500.0,80)
        P10 = Producto(10,"Jamon",15000.0,10)
        self.__productos__[P1.getCodigo()]=P1
        self.__productos__[P2.getCodigo()]=P2
        self.__productos__[P3.getCodigo()]=P3
        self.__productos__[P4.getCodigo()]=P4
        self.__productos__[P5.getCodigo()]=P5
        self.__productos__[P6.getCodigo()]=P6
        self.__productos__[P7.getCodigo()]=P7
        self.__productos__[P8.getCodigo()]=P8
        self.__productos__[P9.getCodigo()]=P9
        self.__productos__[P10.getCodigo()]=P10
        
    def agregar(self, producto):
        result = False
        if not(producto.getCodigo() in self.__productos__):
            self.__productos__[producto.getCodigo()] = producto
            result = True
        return result
    
    def actualizar(self, producto):
        result = False
        if (producto.getCodigo() in self.__productos__):
            self.__productos__[producto.getCodigo()] = producto
            result = True
        return result
    
    def borrar(self, producto):
        result = False
        if (producto.getCodigo() in self.__productos__):
            self.__productos__.pop(producto.getCodigo())
            result = True
        return result
    
    
    def productoMayor(self):
        preciomayor = 0
        answer=''
        for articulo in self.__productos__:
            if self.__productos__[articulo][1]>preciomayor:
                preciomayor = self.__productos__[articulo][1]
                answer = self.__productos__[articulo][0]
        return answer

    def productoMenor(self):
        preciomenor = self.__productos__[list(self.__productos__.keys())[0]][1]
        answer=''
        for articulo in self.__productos__:
            if self.__productos__[articulo][1]<preciomenor:
                preciomenor = self.__productos__[articulo][1]
                answer = self.__productos__[articulo][0]
        return answer

    def promedioPrecios(self):
        answer=0
        for articulo in self.__productos__:
            answer += self.__productos__[articulo][1]
        return answer/len(self.__productos__)

    def valorInventario(self):
        answer=0
        for articulo in self.__productos__:
            answer += (self.__productos__[articulo][1]*self.__productos__[articulo][2])
        return answer
