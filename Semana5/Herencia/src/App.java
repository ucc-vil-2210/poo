public class App {
    public static void main(String[] args) throws Exception {
        Perro firulay = new Perro("Amarillo", 5, 80, "Firulay", "Criollo", "UCC");
        firulay.dormir(true);
        firulay.comer("Sobras");
        firulay.ladrar();
        firulay.sentarse();
        Gato felix = new Gato("negro", 25, 40, "Felix", "Angora", "el mismo");
        felix.sonido();
        firulay.sonido();
    }
}
