from abc import abstractmethod, ABCMeta

class FigurasGeometricas(metaclass=ABCMeta):
    @abstractmethod
    def pintar(self):
        pass
    @abstractmethod
    def area(self):
        pass
    @abstractmethod
    def perimetro(self):
        pass