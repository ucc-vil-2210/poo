public class Fijo extends Empleado{
    //Fijo (hereda de Empleado, atributos: EPS, Fondo de Pensión, Fondo de cesantías, ARL, salario, 
    //      horas extras, método: getPago(número del mes))
    private String EPS;
    private String pension;
    private String cesantias;
    private String arl;
    private int salario;
    private int horasExtras;
    
    //2500000 36 ABC  Coomeva 785458 11 Juan 3539 11
    //Sueldo, HE, codigoint, Banco, cuenta, mes, nombre, id, mesFacturar 
    
    public Fijo(String nombre,  int identificacion,  String codigoInt, String banco,
            int numCuenta, int mesAFacturar,  int salario, int horasExtras) {
        super(nombre, identificacion, codigoInt, banco, numCuenta, mesAFacturar);
        this.salario = salario;
        this.horasExtras = horasExtras;
    }



    public Fijo(String nombre, int identificacion,  String codigoInt, String banco,
            int numCuenta, int mesAFacturar, String ePS, String pension, String cesantias, String arl, int salario,
            int horasExtras) {
        super(nombre, identificacion, codigoInt, banco, numCuenta, mesAFacturar);
        EPS = ePS;
        this.pension = pension;
        this.cesantias = cesantias;
        this.arl = arl;
        this.salario = salario;
        this.horasExtras = horasExtras;
    }



    @Override
    public int getPago(int mes) {
       return (int) (this.salario*0.92+(this.horasExtras*50000));
    }



    public String getEPS() {
        return EPS;
    }



    public void setEPS(String ePS) {
        EPS = ePS;
    }



    public String getPension() {
        return pension;
    }



    public void setPension(String pension) {
        this.pension = pension;
    }



    public String getCesantias() {
        return cesantias;
    }



    public void setCesantias(String cesantias) {
        this.cesantias = cesantias;
    }



    public String getArl() {
        return arl;
    }



    public void setArl(String arl) {
        this.arl = arl;
    }



    public int getSalario() {
        return salario;
    }



    public void setSalario(int salario) {
        this.salario = salario;
    }



    public int getHorasExtras() {
        return horasExtras;
    }



    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }

}
