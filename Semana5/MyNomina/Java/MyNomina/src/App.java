import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Ingrese los datos ");
        Scanner leer = new Scanner(System.in);
        String datos = leer.nextLine();
        String[] entrada = datos.split(" ");
        if (Integer.parseInt(entrada[0])==1){
            //String nombre,  int identificacion,  String codigoInt, String banco,
            //int numCuenta, int mesAFacturar,  int salario, int horasExtras
            //1 2500000 36 ABC  Coomeva 785458 11 Juan 3539 11
            Fijo empleadoFijo = new Fijo(entrada[7],Integer.parseInt(entrada[8]), entrada[3], entrada[4], 
                                     Integer.parseInt(entrada[5]), Integer.parseInt(entrada[6]), 
                                     Integer.parseInt(entrada[1]), Integer.parseInt(entrada[2]));
            if (Integer.parseInt(entrada[6])==Integer.parseInt(entrada[9])){
                System.out.println("El sueldo para el empleado "+entrada[3] +" es "
                                    + empleadoFijo.getPago(Integer.parseInt(entrada[9])));
            }else{
                if (Integer.parseInt(entrada[6])>Integer.parseInt(entrada[9])){
                    System.out.println("Mes ya facturado");
                }else{
                    System.out.println("Mes no facturado aún");
                }
            }
        }else {
            if (Integer.parseInt(entrada[0])==2){
                //String nombre,  int identificacion, String codigoInt, String banco,
                //int numCuenta, int mesAFacturar, int horasTrabajadas, int valorHora
                Proveedor empleadoProv = new Proveedor(entrada[7],Integer.parseInt(entrada[8]), entrada[3], entrada[4], 
                Integer.parseInt(entrada[5]), Integer.parseInt(entrada[6]), 
                Integer.parseInt(entrada[1]), Integer.parseInt(entrada[2]));
                if (Integer.parseInt(entrada[6])==Integer.parseInt(entrada[9])){
                    System.out.println("El sueldo para el empleado "+entrada[3] +" es "
                                        + empleadoProv.getPago(Integer.parseInt(entrada[9])));
                }else{
                    if (Integer.parseInt(entrada[6])>Integer.parseInt(entrada[9])){
                        System.out.println("Mes ya facturado");
                    }else{
                        System.out.println("Mes no facturado aún");
                    }
                }
            }
        } 
    }
}
