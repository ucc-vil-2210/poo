public class Empleado extends Persona{
    //Empleado (implementa de Persona, atributos: código interno, banco, número de cuenta, 
    //          mes a facturar, método: getPago(número del mes))
    private String codigoInt;
    private String banco;
    private int numCuenta;
    private int mesAFacturar;
    public Empleado(String nombre, int identificacion, String codigoInt, String banco,
            int numCuenta, int mesAFacturar) {
        super(nombre, identificacion);
        this.setCodigoInt(codigoInt);
        this.setBanco(banco);
        this.setNumCuenta(numCuenta);
        this.setMesAFacturar(mesAFacturar);
    }
    //codigoint, Banco, cuenta, mes, nombre, id

    public String getCodigoInt() {
        return codigoInt;
    }
    public void setCodigoInt(String codigoInt) {
        this.codigoInt = codigoInt;
    }
    public String getBanco() {
        return banco;
    }
    public void setBanco(String banco) {
        this.banco = banco;
    }
    public int getNumCuenta() {
        return numCuenta;
    }
    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }
    public int getMesAFacturar() {
        return mesAFacturar;
    }
    public void setMesAFacturar(int mesAFacturar) {
        this.mesAFacturar = mesAFacturar;
    }
    
    public int getPago(int mes){
        return 0;
    }
}
