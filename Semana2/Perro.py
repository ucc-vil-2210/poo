class Perro:
    edad = 0
    nombre = ""
    color = ""
    
    def __init__(self, edad, nombre, color):
        self.edad = edad
        self.nombre = nombre
        self.color = color

    def ladrar(self):
        print("Guauuu guauuu...")

MiMascota = Perro(4,"Firulay","Amarillo")
MiMascota.ladrar()
print(MiMascota.nombre)