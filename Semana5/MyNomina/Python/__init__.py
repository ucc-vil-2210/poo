from Fijo import Fijo
from Proveedor import Proveedor

if __name__=='__main__':
    entrada = input("Ingrese los datos: ")
    datos =  entrada.split(' ')
    if (int(datos[0]) == 1):
        if (datos[6]==datos[9]):
            empleadoFijo = Fijo(datos[7], int(datos[8]), datos[3], datos[4], 
                                int(datos[5]), int(datos[6]) , int(datos[1]), 
                                int(datos[2]))
            mes=int(datos[9])
            print("El sueldo para el empleado "+datos[3] +" es "
                                    + str(empleadoFijo.getPago()))
            #String nombre,  int identificacion,  String codigoInt, String banco,
            #int numCuenta, int mesAFacturar,  int salario, int horasExtras
            #1 2500000 36 ABC  Coomeva 785458 11 Juan 3539 11
        elif (datos[6]>datos[9]):
            print("Mes ya facturado")
        else:
            print("Mes sin facturar")
    elif (int(datos[0]) == 2):
        if (datos[6]==datos[9]):
            empleadoProv = Proveedor(datos[7], int(datos[8]), datos[3], datos[4], 
                                int(datos[5]), int(datos[6]) , int(datos[1]), 
                                int(datos[2]))
            mes=int(datos[9])
            print("El sueldo para el empleado "+datos[3] +" es "
                                    + str(empleadoProv.getPago()))
            #String nombre,  int identificacion,  String codigoInt, String banco,
            #int numCuenta, int mesAFacturar,  int salario, int horasExtras
            #1 2500000 36 ABC  Coomeva 785458 11 Juan 3539 11
        elif (datos[6]>datos[9]):
            print("Mes ya facturado")
        else:
            print("Mes sin facturar")