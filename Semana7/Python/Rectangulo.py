from FigurasGeometricas import FigurasGeometricas

class Rectangulo(FigurasGeometricas):
    __altura__ = 0.0
    __base__ = 0.0
    
    def __init__(self, altura, base):
        super().__init__()
        self.__altura__ = altura
        self.__base__ = base
        
    def getAltura(self):
        return self.__altura__
    
    def setAltura(self, altura):
        self.__altura__ = altura
        
    def getBase(self):
        return self.__base__
    
    def setBase(self, base):
        self.__base__ = base
    
    def area(self):
        return self.__base__*self.__altura__
    
    def perimetro(self):
        return (self.__altura__+self.__base__)*2
    
    def pintar(self):
        print("Rectangulo de base "+str(self.__base__) + " altura "+str(self.__altura__))
        
    
    