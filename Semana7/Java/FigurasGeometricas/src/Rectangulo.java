public class Rectangulo implements FiguraGeometrica {

    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double area() {
        // TODO Auto-generated method stub
        return this.base * this.altura;
    }

    @Override
    public double perimetro() {
        // TODO Auto-generated method stub
        return (this.altura + this.base) * 2;
    }

    @Override
    public String pintar() {
        // TODO Auto-generated method stub
        String ans = "Rectangulo de Base " + Double.toString(base) + " y altura " + Double.toString(this.altura);
        System.out.println(ans);
        return ans;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

}
