public class Perro {
    private int id;
    private String nombre;
    private String color;
    private int edad;
    private double altura;

    public Perro(int id, String nombre, String color, int edad, double altura) {
        this.id = id;
        this.nombre = nombre;
        this.color = color;
        this.edad = edad;
        this.altura = altura;
    }

    public Perro() {
        this.id = 0;
        this.nombre = "";
        this.color = "";
        this.edad = 0;
        this.altura = 0;
    }

    public Perro(int id) {
        this.id = id;
        this.nombre = "";
        this.color = "";
        this.edad = 0;
        this.altura = 0;
    }

    public String getNombre() {
        return this.nombre;
    }

    public int getId() {
        return this.id;
    }

    public String getColor() {
        return this.color;
    }

    public int getEdad() {
        return this.edad;
    }

    public double getAltura() {
        return this.altura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public boolean setId(int id) {
        this.id = id;
        return true;
    }

    public void comer() {
        System.out.println("El perro " + this.nombre + " come concentrado");
    }

    public void comer(String comida) {
        System.out.println("El perro " + this.nombre + " come " + comida);
    }

    @Override
    public String toString() {
        return "Perro: " + " Id:" + this.id + ", Nombre:" + this.nombre + ", Color:" + this.color + ", Edad:"
                + this.edad + ", Altura:" + altura;
    }
}