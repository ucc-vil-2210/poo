public class Perro extends Animal {
    private String nombre;
    private String raza;
    private String dueno;

    public Perro(String color, int edad, double altura, String nombre, String raza, String dueno) {
        super(color, edad, altura);
        this.nombre = nombre;
        this.raza = raza;
        this.dueno = dueno;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void ladrar() {
        System.out.println("Guau Guau...Grr");
    }

    public void sentarse() {
        System.out.println(nombre + " esta sentado");
    }

    @Override
    public void sonido() {
        System.out.println(nombre + " Ladra guau guau...");
    }
}
