public class Persona {
    private String nombre;
    private String apellido; 
    private int identificacion;
    private int celular;
    
    public Persona(String nombre, int identificacion) {
        this.nombre = nombre;
        this.identificacion = identificacion;
    }
    public Persona(String nombre, String apellido, int identificacion, int celular) {
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setIdentificacion(identificacion);
        this.setCelular(celular);
    }
    //nombre, id
    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
